#!/bin/bash

# install docker
yum install -y docker && docker -v
if [ $? -ne 0 ]; then
    echo 'ERROR: Install docker failed!!'
    exit $?
fi

# build runner image
docker images | grep eis-runner

if [ $? -ne 0 ]; then
    cd runner && docker bulid -t eis-runner .
    cd ..
else
    echo 'eis runner image exists already.'
fi

# run runners for be and fe
if [ $? -ne 0 ]; then
    echo 'ERROR: please check whether your eis-runner image was created successfully!'
    exit $?
fi

# make dirs
mkdir -p /home/ssl
mkdir -p /home/www/test
mkdir -p /home/www/prod

# mkdir -p /home/db/backups/test
# mkdir -p /home/db/backups/prod
# mkdir -p /home/db/data/test
# mkdir -p /home/db/data/prod

# mkdir -p /home/uploadfiles/test
# mkdir -p /home/uploadfiles/prod
if [ ! -d "/home/db/backups/test" ]; then
    echo 'Path /home/db/backups/test not exists'
    exit -1
fi
if [ ! -d "/home/db/backups/prod" ]; then
    echo 'Path /home/db/backups/prod not exists'
    exit -1
fi
if [ ! -d "/home/db/data/test" ]; then
    echo 'Path /home/db/data/test not exists'
    exit -1
fi
if [ ! -d "/home/db/data/prod" ]; then
    echo 'Path /home/db/data/prod not exists'
    exit -1
fi
if [ ! -d "/home/uploadfiles/test" ]; then
    echo 'Path /home/uploadfiles/test not exists'
    exit -1
fi
if [ ! -d "/home/uploadfiles/test" ]; then
    echo 'Path /home/uploadfiles/test not exists'
    exit -1
fi

docker stack deploy -c ./deploy.yml

# register gitlab runners
echo "Register productioin runner for backend..."
docker exec -it be_prod gitlab-runner register
if [ $? -ne 0 ]; then
    exit $?
fi

echo "Register test runner for backend..."
docker exec -it be_test gitlab-runner register
if [ $? -ne 0 ]; then
    exit $?
fi

echo "Register runner for frontend..."
docker exec -it fe gitlab-runner register
if [ $? -ne 0 ]; then
    exit $?
fi

echo "Please add your runners to the projects."
read -p "Press any to continue." registerFinished